﻿using System;
using UnityEngine;

namespace Ease
{
    [Serializable]
    public struct MoveStruct
    {
        public Vector2 start;
        public Vector2 final;
        public GameObject targetObject;
        public AnimationCurve curve;
    }
}
