﻿using System;
using UnityEngine;

namespace Ease
{
    [Serializable]
    public struct SizeStruct
    {
        public Vector2 start;
        public Vector2 final;
        public AnimationCurve curve;
    }
}
