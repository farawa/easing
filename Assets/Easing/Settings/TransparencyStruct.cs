﻿using System;
using UnityEngine;

namespace Ease
{
    [Serializable]
    public struct TransparencyStruct
    {
        public float start;
        public float final;
        public AnimationCurve curve;
    }
}
