﻿using System;
using UnityEngine;

namespace Ease
{
    [Serializable]
    public struct RotationStruct
    {
        public float startAngle;
        public float finalAngle;
        public AnimationCurve curve;
    }
}