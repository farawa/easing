﻿using Ease;
using System;

namespace Ease
{
    [Serializable]
    public struct EaseSettings
    {
        public ScaleStruct scale;
        public SizeStruct size;
        public RotationStruct rotation;
        public TransparencyStruct transparency;
        public MoveStruct moving;
        public SideMovingStruct secondMoving;
    }
}