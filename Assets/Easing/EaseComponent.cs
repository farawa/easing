﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ease
{
    //[ExecuteInEditMode]
    public class EaseComponent : MonoBehaviour
    {
        [Header("Вход")]
        [SerializeField] private bool isUseScale = false;
        [SerializeField] private bool isUseSize = false;
        [SerializeField] private bool isUseRotation = false;
        [SerializeField] private bool isUseTransparency = false;
        [SerializeField] private bool isUseMoving = false;
        [SerializeField] private bool isUseSecondMoving = false;
        [Header("Выход")]
        [SerializeField] private bool isUseScaleOut = false;
        [SerializeField] private bool isUseSizeOut = false;
        [SerializeField] private bool isUseRotationOut = false;
        [SerializeField] private bool isUseTransparencyOut = false;
        [SerializeField] private bool isUseMovingOut = false;
        [SerializeField] private bool isUseSecondMovingOut = false;
        [Header("Настройки прозрачности")]
        [SerializeField] private TransparencySetting transparencySetting;
        [Header("Скорость")]
        [Range(0.1f, 10f)]
        [SerializeField] private float enterEaseSpeed = 1;
        [Range(0.1f, 10f)]
        [SerializeField] private float outEaseSpeed = 1;
        [Header("Появление")]
        [SerializeField] private EaseSettings easeEnter;
        [Header("Изчезновение")]
        [SerializeField] private EaseSettings easeOut;
        public bool isAnimationPlaying { get; set; } = false;
        //TODO blackList

        public void StartEnter(Action action = null)
        {
            isAnimationPlaying = true;
            if (isUseScale)
                EaseCore.instance.StartEase(gameObject, enterEaseSpeed, easeEnter.scale);

            if (isUseSize)
                EaseCore.instance.StartEase(gameObject, enterEaseSpeed, easeEnter.size);

            if (isUseRotation)
                EaseCore.instance.StartEase(gameObject, enterEaseSpeed, easeEnter.rotation);

            if (isUseTransparency)
            {
                var easeObjects = new List<GameObject>();
                easeObjects.Add(gameObject);
                if (transparencySetting.isChildrensEnter)
                {
                    if (transparencySetting.blackList.Count == 0 && transparencySetting.whiteList.Count == 0)
                        StartAllTransparency(ref easeObjects);
                    else if (transparencySetting.blackList.Count > 0 && transparencySetting.whiteList.Count == 0)
                        StartAllTransparency(ref easeObjects, transparencySetting.blackList.ToArray(), true);
                    else if (transparencySetting.whiteList.Count > 0 && transparencySetting.blackList.Count == 0)
                        StartAllTransparency(ref easeObjects, transparencySetting.whiteList.ToArray(), false);
                    else if (transparencySetting.blackList.Count != 0 && transparencySetting.whiteList.Count != 0)
                        throw new ArgumentException("Добавлены элементы как в черный, так и в белый список, можно использовать только один из них!");
                }
                foreach (var obj in easeObjects)
                    EaseCore.instance.StartEase(obj, enterEaseSpeed, easeEnter.transparency);
                print(easeObjects.Count);
            }

            if (isUseMoving)
                EaseCore.instance.StartEase(gameObject, enterEaseSpeed, easeEnter.moving);

            if (isUseSecondMoving)
            {
                var moving = new MoveStruct();
                moving.start = easeEnter.moving.start + easeEnter.secondMoving.start;
                moving.final = easeEnter.moving.start + easeEnter.secondMoving.final;
                moving.curve = easeEnter.secondMoving.curve;
                EaseCore.instance.StartEase(gameObject, enterEaseSpeed, moving);
            }
            if (action != null)
                SetCallback(action);
        }

        public void StartOut(Action action = null)
        {
            isAnimationPlaying = true;
            if (isUseScaleOut)
                EaseCore.instance.StartEase(gameObject, outEaseSpeed, easeOut.scale);

            if (isUseSizeOut)
                EaseCore.instance.StartEase(gameObject, outEaseSpeed, easeOut.size);

            if (isUseRotationOut)
                EaseCore.instance.StartEase(gameObject, outEaseSpeed, easeOut.rotation);

            if (isUseTransparencyOut)
            {
                var easeObjects = new List<GameObject>();
                easeObjects.Add(gameObject);
                if (transparencySetting.isChildrensEnter)
                {
                    if (transparencySetting.blackList.Count == 0 && transparencySetting.whiteList.Count == 0)
                        StartAllTransparency(ref easeObjects);
                    else if (transparencySetting.blackList.Count > 0 && transparencySetting.whiteList.Count == 0)
                        StartAllTransparency(ref easeObjects, transparencySetting.blackList.ToArray(), true);
                    else if (transparencySetting.whiteList.Count > 0 && transparencySetting.blackList.Count == 0)
                        StartAllTransparency(ref easeObjects, transparencySetting.whiteList.ToArray(), false);
                    else if (transparencySetting.blackList.Count != 0 && transparencySetting.whiteList.Count != 0)
                        throw new ArgumentException("Добавлены элементы как в черный, так и в белый список, можно использовать только один из них!");
                }
                foreach (var obj in easeObjects)
                    EaseCore.instance.StartEase(obj, enterEaseSpeed, easeOut.transparency);
                print(easeObjects.Count);
            }
                

            if (isUseMovingOut)
                EaseCore.instance.StartEase(gameObject, outEaseSpeed, easeOut.moving);

            if (isUseSecondMovingOut)
            {
                var moving = new MoveStruct();
                moving.start = easeOut.moving.start + easeOut.secondMoving.start;
                moving.final = easeOut.moving.start + easeOut.secondMoving.final;
                moving.curve = easeOut.secondMoving.curve;
                EaseCore.instance.StartEase(gameObject, outEaseSpeed, moving);
            }
            if (action != null)
                SetCallback(action);
        }

        public void StartFromCurrentPosition(bool isEnter)
        {

        }

        private void StartAllTransparency(ref List<GameObject> easeObjects, GameObject[] array = null, bool isBlackList = false)
        {

            var images = gameObject.GetComponentsInChildren<Image>();
            var texts = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
            foreach (var img in images)
            {
                var isHave = false;
                foreach (var obj in easeObjects)
                {
                    if (img.gameObject == obj.gameObject)
                    {
                        isHave = true;
                        break;
                    }
                }
                if (!isHave)
                    easeObjects.Add(img.gameObject);
            }
            foreach (var text in texts)
            {
                var isHave = false;
                foreach (var obj in easeObjects)
                {
                    if (text.gameObject == obj.gameObject)
                    {
                        isHave = true;
                        break;
                    }
                }
                if (!isHave)
                    easeObjects.Add(text.gameObject);
            }
            if (array == null) return;

            else if(isBlackList)
            {
                var newArray = new List<GameObject>();

                foreach(var obj in easeObjects)
                {
                    var isCanPast = true;
                    foreach(var black in array)
                    {
                        if (black == obj)
                        {
                            isCanPast = false;
                            break;
                        }
                    }
                    if (isCanPast)
                        newArray.Add(obj);
                }

                easeObjects = newArray;
            }
            else if(!isBlackList)
            {
                var newArray = new List<GameObject>();

                foreach (var obj in easeObjects)
                {
                    var isCanPast = false;
                    foreach (var white in array)
                    {
                        if (white == obj)
                        {
                            isCanPast = true;
                            break;
                        }
                    }
                    if (isCanPast)
                        newArray.Add(obj);
                }

                easeObjects = newArray;
            }
        }

        public void SetCallback(Action action)
        {
            StartCoroutine(WaitEndAnimation(action));
        }

        private IEnumerator WaitEndAnimation(Action action)
        {
            while (true)
            {
                if (isAnimationPlaying == true)
                    yield return new WaitForEndOfFrame();
                else
                    break;
            }
            action?.Invoke();
        }
    }
}