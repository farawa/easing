﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ease
{
    public struct CallbackStruct
    {
        public GameObject go;
        public Action action;
        public EaseType easeType;
    }
}
