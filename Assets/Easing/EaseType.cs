﻿namespace Ease
{
    public enum EaseType
    {
        scale,
        size,
        rotation,
        transparency,
        move,
        all
    }
}
