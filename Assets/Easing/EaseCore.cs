﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ease
{
    //[ExecuteInEditMode]
    public class EaseCore : MonoBehaviour
    {
        public static EaseCore instance = null;
        [SerializeField] private float speedMultiply = 85;
        private List<ObjectCoroutines> easeObjects = new List<ObjectCoroutines>();
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this);
                return;
            }
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }
        //Size
        public void StartEase(GameObject go, float speed, SizeStruct sizeStruct, Action callback = null)//TODO start protected
        {
            var c = StartCoroutine(StartSizeCoroutine(go, speed, sizeStruct));
            AddCoroutineObject(c, go, EaseType.size, callback);
            AddCallback(go, callback, EaseType.size);
        }
        //Scale
        public void StartEase(GameObject go, float speed, ScaleStruct scaleStruct, Action callback = null)
        {
            var c = StartCoroutine(StartScaleCoroutine(go, speed, scaleStruct));
            AddCoroutineObject(c, go, EaseType.scale, callback);
            AddCallback(go, callback, EaseType.scale);
        }
        //Rotation
        public void StartEase(GameObject go, float speed, RotationStruct rotationStruct, Action callback = null)
        {
            var c = StartCoroutine(StartRotationCoroutine(go, speed, rotationStruct));
            AddCoroutineObject(c, go, EaseType.rotation, callback);
            AddCallback(go, callback, EaseType.rotation);
        }
        //Move
        public void StartEase(GameObject go, float speed, MoveStruct movingStruct, Action callback = null)
        {
            Coroutine c;
            if (movingStruct.targetObject == null)
                c = StartCoroutine(StartMovingCoroutineAnchored(go, speed, movingStruct));
            else
                c = StartCoroutine(StartMovingCoroutineWorld(go, speed, movingStruct));
            AddCoroutineObject(c, go, EaseType.move, callback);
            AddCallback(go, callback, EaseType.move);
        }
        //Transparency
        public void StartEase(GameObject go, float speed, TransparencyStruct transparencyStruct, Action callback = null)
        {
            var c = StartCoroutine(StartTransparencyCoroutine(go, speed, transparencyStruct));
            AddCoroutineObject(c, go, EaseType.transparency, callback);
            AddCallback(go, callback, EaseType.transparency);
        }
        //StopCoroutines
        private void StopCoroutine(GameObject go, EaseType easeType, bool isCallCallback = false)
        {
            for (int i = 0; i < easeObjects.Count; i++)
            {
                if (go == easeObjects[i].gameObject)
                {
                    for (int j = easeObjects[i].coroutines.Count - 1; j >= 0; j--)
                    {
                        if (easeObjects[i].coroutines[j].easeType == easeType || easeType == EaseType.all)
                        {
                            StopCoroutine(easeObjects[i].coroutines[j].coroutine);
                            if (isCallCallback) easeObjects[i].coroutines[j].callback?.Invoke();
                            easeObjects[i].coroutines.RemoveAt(j);
                        }
                    }
                    if (easeObjects[i].coroutines.Count == 0)
                    {
                        easeObjects.RemoveAt(i);
                        var easeComponent = go.GetComponent<EaseComponent>();
                        if (easeComponent != null)
                            easeComponent.isAnimationPlaying = false;
                    }
                    break;
                }
            }
        }
        //Callback
        private void AddCallback(GameObject go, Action action, EaseType easeType)
        {
            if (action == null) return;
            ObjectCoroutines oc = new ObjectCoroutines();
            var isDone = TryGetCoroutine(gameObject, ref oc);
            if (isDone)
            {
                for (int i = 0; i < oc.coroutines.Count; i++)
                {
                    if (oc.coroutines[i].easeType == easeType)
                    {
                        var t = oc.coroutines[i];
                        t.callback += action;
                        oc.coroutines[i] = t;
                        break;
                    }
                }
            }
            else
            {
                throw new Exception();
            }
        }

        private void AddCoroutineObject(Coroutine coroutine, GameObject gameObject, EaseType easeType, Action callback)
        {
            ObjectCoroutines oc = new ObjectCoroutines();
            var isDone = TryGetCoroutine(gameObject, ref oc);
            if (isDone)
            {
                EaseCoroutine t = new EaseCoroutine()
                {
                    coroutine = coroutine,
                    easeType = easeType,
                    callback = callback
                };
            }
            else
            {
                oc.gameObject = gameObject;
                EaseCoroutine t = new EaseCoroutine()
                {
                    easeType = easeType,
                    coroutine = coroutine,
                    callback = callback
                };
                if (oc.coroutines == null) oc.coroutines = new List<EaseCoroutine>();
                oc.coroutines.Add(t);
                easeObjects.Add(oc);
            }
        }

        private bool TryGetCoroutine(GameObject gameObject, ref ObjectCoroutines objectCoroutine)
        {
            foreach (var obj in easeObjects)
            {
                if (obj.gameObject == gameObject)
                {
                    objectCoroutine = obj;
                    return true;
                }
            }
            return false;
        }

        public IEnumerator StartScaleCoroutine(GameObject go, float speed, ScaleStruct scaleStruct)
        {
            float i = 0;

            while (true)
            {
                if (go == null) yield break;
                if (i > 100) i = 100;

                var difference = scaleStruct.final - scaleStruct.start;
                var curveValue = scaleStruct.curve.Evaluate(i / 100);
                var v3 = new Vector3(difference.x, difference.y, 1);
                var v3Start = new Vector3(scaleStruct.start.x, scaleStruct.start.y, 0);
                go.transform.localScale = v3Start + (v3 * curveValue);

                if (i == 100) break;
                else i += speed * speedMultiply * Time.deltaTime;
                yield return null;
            }
            EndAnimation(go, EaseType.scale);
            yield break;
        }

        public IEnumerator StartSizeCoroutine(GameObject go, float speed, SizeStruct scaleStruct)
        {
            float i = 0;

            while (true)
            {
                if (go == null) yield break;
                if (i > 100) i = 100;

                var difference = scaleStruct.final - scaleStruct.start;
                var curveValue = scaleStruct.curve.Evaluate(i / 100);
                var v3 = new Vector3(difference.x, difference.y, 1);
                var v3Start = new Vector3(scaleStruct.start.x, scaleStruct.start.y, 0);
                (go.transform as RectTransform).sizeDelta = v3Start + (v3 * curveValue);

                if (i == 100) break;
                else i += speed * speedMultiply * Time.deltaTime;
                yield return null;
            }
            EndAnimation(go, EaseType.size);
            yield break;
        }

        public IEnumerator StartRotationCoroutine(GameObject go, float speed, RotationStruct rotationStruct)
        {
            float i = 0;

            while (true)
            {
                if (go == null) yield break;
                if (i > 100) i = 100;

                var difference = rotationStruct.finalAngle - rotationStruct.startAngle;
                var curveValue = rotationStruct.curve.Evaluate(i / 100);
                go.transform.eulerAngles = new Vector3(0, 0, rotationStruct.startAngle + (difference * curveValue));

                if (i == 100) break;
                else i += speed * speedMultiply * Time.deltaTime;
                yield return null;
            }
            EndAnimation(go, EaseType.rotation);
            yield break;
        }

        public IEnumerator StartTransparencyCoroutine(GameObject go, float speed, TransparencyStruct transparencyStruct)
        {
            float index = 0;

            Image image = go.GetComponent<Image>();
            TextMeshProUGUI text = go.GetComponent<TextMeshProUGUI>();

            Color color;

            while (true)
            {
                if (go == null) yield break;
                if (index > 100) index = 100;

                var difference = transparencyStruct.final - transparencyStruct.start;
                var curveValue = transparencyStruct.curve.Evaluate(index / 100);

                var value = transparencyStruct.start + (difference * curveValue);
                if (image != null)
                {
                    color = image.color;
                    color.a = value;
                    image.color = color;
                }

                if (text != null)
                {
                    color = text.color;
                    color.a = value;
                    text.color = color;
                }

                if (index == 100) break;
                else index += speed * speedMultiply * Time.deltaTime;
                yield return null;
            }
            EndAnimation(go, EaseType.transparency);
            yield break;
        }

        public IEnumerator StartMovingCoroutineAnchored(GameObject go, float speed, MoveStruct movingStruct)
        {
            float i = 0;
            (go.transform as RectTransform).anchoredPosition3D = movingStruct.start;
            var vector = movingStruct.final - movingStruct.start;
            var currentVector = movingStruct.start;
            while (true)
            {
                if (go == null) yield break;
                if (i > 100) i = 100;

                var curveValue = movingStruct.curve.Evaluate(i / 100);
                var targetVector = curveValue * vector + movingStruct.start;
                (go.transform as RectTransform).anchoredPosition += targetVector - currentVector;
                currentVector = targetVector;

                if (i == 100) break;
                i += speed * speedMultiply * Time.deltaTime;
                yield return null;
            }
            EndAnimation(go, EaseType.move);
            yield break;
        }

        public IEnumerator StartMovingCoroutineWorld(GameObject go, float speed, MoveStruct movingStruct)
        {
            float i = 0;
            (go.transform as RectTransform).anchoredPosition = movingStruct.start;
            var startPos = go.transform.position;
            var finalPos = movingStruct.targetObject.transform.position;
            var vector = finalPos - startPos;
            var currentVector = startPos;

            while (true)
            {
                if (go == null) yield break;
                if (i > 100) i = 100;

                var curveValue = movingStruct.curve.Evaluate(i / 100);
                var targetVector = curveValue * vector + startPos;
                go.transform.position += targetVector - currentVector;
                currentVector = targetVector;

                if (i == 100) break;
                else i += speed * speedMultiply * Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            EndAnimation(go, EaseType.move);
            yield break;
        }

        private void EndAnimation(GameObject go, EaseType easeType)
        {
            StopCoroutine(go, easeType, true);
        }
    }
}
