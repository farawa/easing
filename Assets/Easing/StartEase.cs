﻿using Ease;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartEase : MonoBehaviour, IPointerClickHandler
{
    public EaseComponent[] eases;
    public bool isEnterEase = true;

    public void OnPointerClick(PointerEventData eventData)
    {
        for (int i = 0; i < eases.Length; i++)
        {
            if (eases[i] == null) continue;
            if (isEnterEase)
                eases[i].StartEnter();
            else
                eases[i].StartOut();
        }
    }
}
